//
//  HomeViewController.swift
//  ProLBku
//
//  Created by mac on 29/04/18.
//  Copyright © 2018 irwan. All rights reserved.
//



import UIKit
import Alamofire
import SwiftyJSON
import AlamofireImage
import Foundation

class HomeViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    var arrRes = [[String:AnyObject]]()
    var News = [[String:AnyObject]]()
    var productArray = NSArray()
    typealias JSONDictionary = [String:Any]
    var responseArray = [JSONDictionary]()
    var events: [Event]?
    @IBOutlet weak var imageScroller: ImageScroller!
    var titleArray = [String]()
    @IBOutlet weak var EventCollection: UICollectionView!
    @IBOutlet weak var NewsCollection: UICollectionView!
   @IBOutlet weak var pageControl: UIPageControl!
    
   var sampleImages = ["image1.jpg","image1.jpg","image1.jpg","image1.jpg","image1.jpg"]
    override func viewDidLoad() {
        super.viewDidLoad()
        addNavBarImage()
        getAllNews()
        getAllEvents()
        imageScroller.delegate = self
        imageScroller.isAutoScrollEnabled = true
        imageScroller.scrollTimeInterval = 2.0 //time interval
        imageScroller.scrollView.bounces = false
        imageScroller.setupScrollerWithImages(images: sampleImages)
        imageScroller.isAutoScrollEnabled = true
        self.pageChanged(index: 0)
        
        

    }

   
    

    
   
    
  
    
    func addNavBarImage() {
        
        let navController = navigationController!
        
        let image = UIImage(named: "logo") 
        let imageView = UIImageView(image: image)
        
        let bannerWidth = navController.navigationBar.frame.size.width
        let bannerHeight = navController.navigationBar.frame.size.height
        
        let bannerX = bannerWidth / 2 - (image?.size.width)! / 2
        let bannerY = bannerHeight / 2 - (image?.size.height)! / 2
        
        imageView.frame = CGRect(x: bannerX, y: bannerY, width: 150, height: bannerHeight)
        imageView.contentMode = .scaleAspectFit
        
        navigationItem.titleView = imageView
        
        
        let logo = UIBarButtonItem(image: UIImage (named: "notificationsActiveMaterial")?.withRenderingMode(.alwaysOriginal), style: UIBarButtonItemStyle.plain, target: self, action: nil)
        
        self.navigationItem.rightBarButtonItem = logo
    }
    
 



    func getAllEvents() {
        
        
        let headers: HTTPHeaders = [
            //            "Authorization": "Bearer \(value!)",
            "Accept": "application/json"
        ]
        
        let url = "\(Endpoints.BASE)\(Endpoints.EVENT)"
        
        Alamofire.request(url).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                
                if let resData = swiftyJsonVar["lautan_berlian"]["result"]["events"].arrayObject {
                    self.arrRes = resData as! [[String:AnyObject]]
                }
                if self.arrRes.count > 0 {
                    self.EventCollection.reloadData()
                }
            }
        }
        
        
    }
    

    func getAllNews() {
        
        
        let headers: HTTPHeaders = [
            //            "Authorization": "Bearer \(value!)",
            "Accept": "application/json"
        ]
        
        let url = "https://www.lautanberlian.co.id/wp-json/lbcrm/v1/posts"
        
        Alamofire.request(url).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                
                if let resData = swiftyJsonVar["lautan_berlian"]["result"]["news"].arrayObject {
                    self.News = resData as! [[String:AnyObject]]
                    
                }
                if self.News.count > 0 {
                    self.NewsCollection.reloadData()
                }
            }
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        var count = 0
        if (collectionView == self.EventCollection) {
            count = self.arrRes.count
        }
        else if (collectionView == self.NewsCollection){
            count = self.News.count
        }
        
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        
        if (collectionView == self.EventCollection) {
            let cell:HomeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "EventCell", for: indexPath) as! HomeCollectionViewCell
           
            var dict = arrRes[indexPath.row]
            cell.titleNews.text = dict["title"] as? String

            let addedDate = dict["date"] as? String
            
            let convert = addedDate?.toDateString(inputFormat: addedDate!, outputFormat: "dd MMMM")!
            cell.dateNews.text = convert
            

            return cell
        } else  {
            let cell2:NewsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "newsCell", for: indexPath) as! NewsCollectionViewCell
            var dict = News[indexPath.row]
            cell2.titleEvent.text = dict["title"] as? String
            let addedDate = dict["date"] as? String
            let convert = addedDate?.toDateString(inputFormat: addedDate!, outputFormat: "dd MMMM")!
            cell2.dateEvent.text = convert
            cell2.contentEvent.text = dict["content"] as? String
            let imageTumb = dict["thumbnail_url"] as? String
            cell2.imageEvent.af_setImage(withURL: URL(string: (imageTumb)!)!)
            return cell2
        }

        
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (collectionView == self.EventCollection) {
           
            var selectedItem = indexPath
            var dict = arrRes[selectedItem.row]
            let selectID = dict["ID"] as? Int
            print("You selected cell #\(selectID!)!")
            let signUp = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailViewController") as! EventDetailViewController
            signUp.IDEVENT = selectID
            self.present(signUp, animated: true, completion: nil)
        } else  {
           
            var selectedItem = indexPath
            var dict = News[selectedItem.row]
            let selectID = dict["ID"] as? Int
            print("You selected cell #\(selectID!)!")
            let signUp = self.storyboard?.instantiateViewController(withIdentifier: "DetailBeritaViewController") as! DetailBeritaViewController
            signUp.IDNews = selectID
            self.present(signUp, animated: true, completion: nil)
        }
    }
    @IBAction func goToKenPenumpang(_ sender: Any) {

        let signUp = self.storyboard?.instantiateViewController(withIdentifier: "ProductViewController") as! ProductViewController
        signUp.jenisKendaraan = "penumpang"
        self.present(signUp, animated: true, completion: nil)
    }
    
    @IBAction func goToNiagaRingan(_ sender: Any) {
        let signUp = self.storyboard?.instantiateViewController(withIdentifier: "ProductViewController") as! ProductViewController
        signUp.jenisKendaraan = "niaga-berat"
        self.present(signUp, animated: true, completion: nil)
    }
    
    @IBAction func goToBus(_ sender: Any) {
        let signUp = self.storyboard?.instantiateViewController(withIdentifier: "ProductViewController") as! ProductViewController
        signUp.jenisKendaraan = "bus"
        self.present(signUp, animated: true, completion: nil)
    }
    
    @IBAction func goToNiagaTruck(_ sender: Any) {
        let signUp = self.storyboard?.instantiateViewController(withIdentifier: "ProductViewController") as! ProductViewController
        signUp.jenisKendaraan = "truck"
        self.present(signUp, animated: true, completion: nil)
    }
    
    @IBAction func goToPoint(_ sender: Any) {
        let signUp = self.storyboard?.instantiateViewController(withIdentifier: "PointViewController") as! PointViewController
        self.present(signUp, animated: true, completion: nil)
    }
    
    @IBAction func goToAllEvent(_ sender: Any) {
        let signUp = self.storyboard?.instantiateViewController(withIdentifier: "EventViewController") as! EventViewController
        self.present(signUp, animated: true, completion: nil)
    }
    
    @IBAction func goToAllBerita(_ sender: Any) {
        let signUp = self.storyboard?.instantiateViewController(withIdentifier: "BeritaViewController") as! BeritaViewController
        self.present(signUp, animated: true, completion: nil)
    }
    
    @IBAction func contactUs(_ sender: Any) {
        let signUp = self.storyboard?.instantiateViewController(withIdentifier: "ContactUSViewController") as! ContactUSViewController
        self.present(signUp, animated: true, completion: nil)
    }
}

extension HomeViewController : ImageScrollerDelegate{
    
    func pageChanged(index: Int) {
//        self.pageIndicatorLabel.text = String(format: "%d/%d", index+1,self.sampleImages.count)
//        print(index+1)
        self.pageControl.currentPage = index
    }
}
