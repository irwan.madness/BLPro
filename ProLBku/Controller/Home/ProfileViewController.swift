//
//  ProfileViewController.swift
//  ProLBku
//
//  Created by mac on 01/07/18.
//  Copyright © 2018 irwan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireImage
class ProfileViewController: UIViewController {

    @IBOutlet weak var imageUser: UIImageView!
    @IBOutlet weak var nameUser: UILabel!
    @IBOutlet weak var emailUser: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.set(nil, forKey: UserDefaultConstant.ACCESS_TOKEN)
//        getUserProfile()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func getUserProfile() {
        let value = UserDefaults.standard.string(forKey: UserDefaultConstant.ACCESS_TOKEN)
        print("Bearer>>>>>>>>>>>>>>>> \(value!) <<<<<<<<<<<<")
        let headers: HTTPHeaders = [
            "Authorization": "\(value!)",
            "Accept": "application/json"
        ]
        let url = "\(Endpoints.BASE)\(Endpoints.USER_PROFILE)"
        print(url)
        Alamofire.request(url, method: .get, headers: headers).responseJSON  { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                print(swiftyJsonVar)
                let firstName = swiftyJsonVar["lautan_berlian"]["result"]["profile"]["firts_name"].string
                let lastName = swiftyJsonVar["lautan_berlian"]["result"]["profile"]["last_name"].string
                self.nameUser.text = "\(firstName ?? "") \(lastName ?? "")"
                self.emailUser.text = swiftyJsonVar["lautan_berlian"]["result"]["profile"]["email"].string
                let avatar = swiftyJsonVar["lautan_berlian"]["result"]["profile"]["avatar"].string
                self.imageUser.af_setImage(withURL: URL(string: (avatar)!)!)
                self.imageUser.layer.cornerRadius = self.imageUser.frame.height / 2
                self.imageUser.clipsToBounds = true
            }
        }
    }

}
