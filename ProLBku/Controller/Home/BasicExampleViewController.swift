//
//  BasicExampleViewController.swift
//  ExpyTableView
//
//  Created by Okhan on 16/06/2017.
//  Copyright © 2017 CocoaPods. All rights reserved.
//

import UIKit
import ExpyTableView
import Alamofire
import SwiftyJSON
class BasicExampleViewController: UIViewController {
	
	@IBOutlet weak var expandableTableView: ExpyTableView!
	var productArray: [JSON!] = []
	override func viewDidLoad() {
		super.viewDidLoad()
		expandableTableView.dataSource = self
		expandableTableView.delegate = self //Optional. In this example we use didSelectRowAtIndexPath to solve a known problem. Sepearator disapperance.
        
        let url = "\(Endpoints.BASE)\(Endpoints.JARINGAN_CABANG)"
        Alamofire.request(url).responseJSON { (response) -> Void in
            if let data = response.data {
                guard let json = try? JSON(data: data) else { return }
                self.productArray = json.arrayValue //productArray type must be
                self.expandableTableView.reloadData()

            }
            
           
        }
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		navigationItem.title = "Jaringan"
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
	}
}

//All you have to do is to implement this method. If you have a current table view which is not expandable, you can turn it into an expandable table view just by copying and pasting the code for first cell from cellForRowAtIndexPath method.

extension BasicExampleViewController: ExpyTableViewDataSource {
	func tableView(_ tableView: ExpyTableView, expandableCellForSection section: Int) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HeaderTableViewCell.self)) as! HeaderTableViewCell
		//Make your customizations here.
         let dic = productArray[section]
		cell.labelHeader.text = dic?["name"].string
		return cell
	}
}

//MARK: Basic Table View Implementation, no need to write UITableViewDataSource because ExpyTableViewDataSource is forwarding all the delegate methods of UITableView that are not handled by itself.

extension BasicExampleViewController {
	func numberOfSections(in tableView: UITableView) -> Int {
		return self.productArray.count
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		// Please see https://github.com/okhanokbay/ExpyTableView/issues/12
		// The cell instance that you return from expandableCellForSection: data source method is actually the first row of belonged section. Thus, when you return 4 from numberOfRowsInSection data source method, first row refers to expandable cell and the other 3 rows refer to other rows in this section.
		// So, always return the total row count you want to see in that section
		
		return productArray[section]?["cabang"].arrayValue.count ?? 0
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

			let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FirstTableViewCell.self)) as! FirstTableViewCell
            let dataarry = productArray[indexPath.section]?["cabang"].arrayValue
            let nama = dataarry?[indexPath.row]
			cell.labelFirst.text = nama?["name"].string
            cell.btnaction.addTarget(self, action: #selector(BasicExampleViewController.ButtonClicked(sender:)), for: UIControlEvents.touchUpInside)
			return cell

	}
    
    @objc func ButtonClicked(sender:UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: self.expandableTableView)
        let indexPath = self.expandableTableView.indexPathForRow(at: buttonPosition)
        if indexPath != nil {
            let dataarry = productArray[indexPath!.section]?["cabang"].arrayValue
            let nama = dataarry?[indexPath!.row]
            let IDCabang = nama?["ID"].int
            print(IDCabang!)
            let x : Int = IDCabang!
            let myString = String(x)
            let signUp = self.storyboard?.instantiateViewController(withIdentifier: "CabangViewController") as! CabangViewController
            signUp.IDCabang = myString
            self.present(signUp, animated: true, completion: nil)
        }
    }
}

extension BasicExampleViewController: ExpyTableViewDelegate {
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		//If you don't deselect the row here, seperator of the above cell of the selected cell disappears.
		//Check here for detail: https://stackoverflow.com/questions/18924589/uitableviewcell-separator-disappearing-in-ios7
		
//        tableView.deselectRow(at: indexPath, animated: false)
//        print("DID SELECT row: \(indexPath.row), section: \(indexPath.section)")
//        
//        let dataarry = productArray[indexPath.section]?["cabang"].arrayValue
//        let nama = dataarry?[indexPath.row]
//        print("Button tapped at indexPath \(nama!)")
//        if indexPath.row == indexPath.row {
//            let signUp = self.storyboard?.instantiateViewController(withIdentifier: "CabangViewController") as! CabangViewController
//            //        signUp.IDEVENT = selectID
//            self.present(signUp, animated: true, completion: nil)
//        }
        
	}
}

class FirstTableViewCell: UITableViewCell {
	@IBOutlet weak var labelFirst: UILabel!
    @IBOutlet weak var btnaction: UIButton!
}
class SecondTableViewCell: UITableViewCell {
	@IBOutlet weak var labelSecond: UILabel!
}
class ThirdTableViewCell: UITableViewCell {
	@IBOutlet weak var labelThird: UILabel!
}
class HeaderTableViewCell: UITableViewCell {
	@IBOutlet weak var labelHeader: UILabel!
}
