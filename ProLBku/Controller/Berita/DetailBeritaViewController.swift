//
//  DetailBeritaViewController.swift
//  ProLBku
//
//  Created by mac on 17/07/18.
//  Copyright © 2018 irwan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireImage

class DetailBeritaViewController: UIViewController {

    @IBOutlet weak var newsImg: UIImageView!
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var newsLocation: UILabel!
    @IBOutlet weak var newsDate: UILabel!
    @IBOutlet weak var newsTime: UILabel!
    @IBOutlet weak var newscontent: UILabel!
    var IDNews: Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        getBerita()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func getBerita() {
        
        let ID_ = IDNews ?? 0
        print(ID_)
        let url = "\(Endpoints.BASE)\(Endpoints.News_DETAIL)/\(ID_)"
        print(url)
        Alamofire.request(url, method: .get).responseJSON  { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                print(swiftyJsonVar)
                
                let title = swiftyJsonVar["lautan_berlian"]["result"]["news"]["title"].string
                //                let location = swiftyJsonVar["lautan_berlian"]["result"]["news"]["event_location"].string
                //                let event_start = swiftyJsonVar["lautan_berlian"]["result"]["news"]["event_start"].string
                //                let event_end = swiftyJsonVar["lautan_berlian"]["result"]["news"]["event_end"].string
                let content = swiftyJsonVar["lautan_berlian"]["result"]["news"]["content"].string
                let thumbnail_url = swiftyJsonVar["lautan_berlian"]["result"]["news"]["thumbnail_url"].string
                
                self.newsTitle.text = title
                self.newscontent.text = content
                if thumbnail_url != nil {
                self.newsImg.af_setImage(withURL: URL(string: (thumbnail_url)!)!)
                }
            }
        }
        
        
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
