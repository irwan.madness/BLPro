//
//  BeritaViewController.swift
//  ProLBku
//
//  Created by mac on 05/07/18.
//  Copyright © 2018 irwan. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire
import SwiftyJSON
class BeritaViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var arrRes = [[String:AnyObject]]()
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            self.tableView.dataSource = self
            self.tableView.delegate = self
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        getEvent()
        // Do any additional setup after loading the view.
    }

    func getEvent() {
        
        
        let headers: HTTPHeaders = [
            //            "Authorization": "Bearer \(value!)",
            "Accept": "application/json"
        ]
        
        let url = "\(Endpoints.BASE)\(Endpoints.NEWS)"
        
        Alamofire.request(url).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                
                if let resData = swiftyJsonVar["lautan_berlian"]["result"]["news"].arrayObject {
                    self.arrRes = resData as! [[String:AnyObject]]
                }
                if self.arrRes.count > 0 {
                    self.tableView.reloadData()
                }
            }
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrRes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cells", for: indexPath as IndexPath) as! NewsTableViewCell
        var dict = arrRes[indexPath.row]
        let imageTumb = dict["thumbnail_url"] as? String
        cell.titleNews.text =  dict["title"] as? String
        cell.detailNews.text = dict["content"] as? String
        cell.dateNews.text = dict["date"] as? String
        cell.imageNews.af_setImage(withURL: URL(string: (imageTumb)!)!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var selectedItem = indexPath
        var dict = arrRes[selectedItem.row]
        let selectID = dict["ID"] as? Int
        print("You selected cell #\(selectID!)!")
        let signUp = self.storyboard?.instantiateViewController(withIdentifier: "DetailBeritaViewController") as! DetailBeritaViewController
        signUp.IDNews = selectID
        self.present(signUp, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
