//
//  ProductTableViewCell.swift
//  ProLBku
//
//  Created by mac on 03/07/18.
//  Copyright © 2018 irwan. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell {

    @IBOutlet weak var dateEvent: UILabel!
    @IBOutlet weak var titleEvent: UILabel!
    @IBOutlet weak var timeEvent: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
