//
//  KendaraanTableViewCell.swift
//  ProLBku
//
//  Created by mac on 26/09/18.
//  Copyright © 2018 irwan. All rights reserved.
//

import UIKit

class KendaraanTableViewCell: UITableViewCell {

    @IBOutlet weak var imageKendaraan: UIImageView!
    @IBOutlet weak var namaKendaraan: UILabel!
    @IBOutlet weak var descKendaraan: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
