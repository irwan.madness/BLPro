//
//  NewsCollectionViewCell.swift
//  ProLBku
//
//  Created by mac on 29/04/18.
//  Copyright © 2018 irwan. All rights reserved.
//

import UIKit

class NewsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleEvent: UILabel!
    @IBOutlet weak var imageEvent: UIImageView!
    @IBOutlet weak var contentEvent: UILabel!
    @IBOutlet weak var dateEvent: UILabel!
}
