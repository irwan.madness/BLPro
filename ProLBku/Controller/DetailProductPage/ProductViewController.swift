//
//  ProductViewController.swift
//  ProLBku
//
//  Created by mac on 03/07/18.
//  Copyright © 2018 irwan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ProductViewController: UIViewController , UITableViewDelegate, UITableViewDataSource{
   
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            self.tableView.dataSource = self
            self.tableView.delegate = self
        }
    }

    @IBOutlet weak var titleLabel: UILabel!
    var jenisKendaraan: String?
    var Kendaraan = [[String:AnyObject]]()
    var imageKendaraan = [[String:AnyObject]]()
    override func viewDidLoad() {
        super.viewDidLoad()
        getKendaraan()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getKendaraan() {
        
        
        let headers: HTTPHeaders = [
            //            "Authorization": "Bearer \(value!)",
            "Accept": "application/json"
        ]
        print(jenisKendaraan!)
        self.titleLabel.text = jenisKendaraan!
        let url = "\(Endpoints.BASE)\(Endpoints.KENDARAAN)\(jenisKendaraan!)"
        
        Alamofire.request(url).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                
                if let resData = swiftyJsonVar["lautan_berlian"]["result"]["cars"].arrayObject {
                    self.Kendaraan = resData as! [[String:AnyObject]]
//                    print(resData)
                    
                }
                
                if let resData1 = swiftyJsonVar["lautan_berlian"]["result"]["cars"]["feature"].arrayObject {
//                    self.Kendaraan = resData as! [[String:AnyObject]]
                                        print(resData1)
                    
                }
                
                if self.Kendaraan.count > 0 {
                    self.tableView.reloadData()
                }
            }
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.Kendaraan.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cells", for: indexPath as IndexPath) as! KendaraanTableViewCell
        cell.selectionStyle = .none
        var dict = Kendaraan[indexPath.row]
        cell.namaKendaraan.text = dict["model"] as? String
        cell.descKendaraan.text = dict["description"] as? String
        let gambar = dict["feature"] as? [[String:AnyObject]]
        if let categoryresult = gambar {
            
            if categoryresult.count > 0 && categoryresult[0]["image"] != nil {
                print("bannerresult  \(categoryresult[0]["image"]!)")
                let imageTumb = categoryresult[0]["image"]
                cell.imageKendaraan.af_setImage(withURL: URL(string: (imageTumb)! as! String)!)
            }
            
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NSLog("You selected cell number: \(indexPath.row)!")
        let signUp = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailViewController") as! ProductDetailViewController
        var dict = Kendaraan[indexPath.row]
        signUp.modelKendaraan = dict["ID"] as? Int
        self.present(signUp, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
