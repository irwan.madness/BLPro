//
//  ProductDetailViewController.swift
//  ProLBku
//
//  Created by mac on 28/09/18.
//  Copyright © 2018 irwan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ProductDetailViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet weak var WarnaCollectionView: UICollectionView!
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    var modelKendaraan: Int?
    var Kendaraan = [[String:AnyObject]]()
    override func viewDidLoad() {
        super.viewDidLoad()
        getDetailKendaraan()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func getDetailKendaraan() {
        
        
        let headers: HTTPHeaders = [
            //            "Authorization": "Bearer \(value!)",
            "Accept": "application/json"
        ]
        print(modelKendaraan!)
//        self.titleLabel.text = modelKendaraan!
        let url = "\(Endpoints.BASE)\(Endpoints.KENDARAAN_DETAIL)\(modelKendaraan!)"
        
        Alamofire.request(url).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                self.modelLabel.text = swiftyJsonVar["lautan_berlian"]["result"]["car"]["name"].string
                self.descLabel.text = swiftyJsonVar["lautan_berlian"]["result"]["car"]["description"].string
                
//                if let resData = swiftyJsonVar["lautan_berlian"]["result"]["cars"].arrayObject {
////                    self.Kendaraan = resData as! [[String:AnyObject]]
//                    //                    print(resData)
//
//                }
                
                if let resData = swiftyJsonVar["lautan_berlian"]["result"]["car"]["images"].arrayObject {
                    self.Kendaraan = resData as! [[String:AnyObject]]
                    

                }
                
                if self.Kendaraan.count > 0 {
                    self.WarnaCollectionView.reloadData()
                }
            }
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.Kendaraan.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:WarnaCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "WarnaCollectionViewCell", for: indexPath) as! WarnaCollectionViewCell
        var dict = Kendaraan[indexPath.row]
         let imageTumb = dict["image"] as? String
//        print(imageTumb)
        cell.imageCars.af_setImage(withURL: URL(string: (imageTumb)! as! String)!)
        return cell
    }
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
