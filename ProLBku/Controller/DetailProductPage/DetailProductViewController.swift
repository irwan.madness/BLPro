//
//  DetailProductViewController.swift
//  ProLBku
//
//  Created by reinnatan on 21/06/18.
//  Copyright © 2018 irwan. All rights reserved.
//

import UIKit



class DetailProductViewController: UIViewController {
    
    @IBOutlet weak var testDriveButton: UILabel!
    @IBOutlet weak var simKreditButton: UILabel!
    @IBOutlet weak var downloadBrosurButton: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.testDriveButton.layer.masksToBounds =  true
        self.testDriveButton.layer.borderWidth =  1
        self.simKreditButton.layer.masksToBounds =  true
        
        self.simKreditButton.layer.borderWidth = 1
        self.downloadBrosurButton.layer.masksToBounds =  true
        
        self.downloadBrosurButton.layer.borderWidth = 1
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
