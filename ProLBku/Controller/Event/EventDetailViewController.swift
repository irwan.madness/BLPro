//
//  EventDetailViewController.swift
//  ProLBku
//
//  Created by mac on 15/07/18.
//  Copyright © 2018 irwan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireImage
class EventDetailViewController: UIViewController {

    @IBOutlet weak var eventImg: UIImageView!
    @IBOutlet weak var eventTitle: UILabel!
    @IBOutlet weak var eventLocation: UILabel!
    @IBOutlet weak var eventDate: UILabel!
    @IBOutlet weak var eventTime: UILabel!
    @IBOutlet weak var eventcontent: UILabel!
    var IDEVENT: Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getNews()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    func getNews() {
        
        let ID_ = IDEVENT ?? 0
        print(ID_)
        let url = "\(Endpoints.BASE)\(Endpoints.News_DETAIL)/\(ID_)"
        print(url)
        Alamofire.request(url, method: .get).responseJSON  { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
//                print(swiftyJsonVar)

                let title = swiftyJsonVar["lautan_berlian"]["result"]["news"]["title"].string
//                let location = swiftyJsonVar["lautan_berlian"]["result"]["news"]["event_location"].string
//                let event_start = swiftyJsonVar["lautan_berlian"]["result"]["news"]["event_start"].string
//                let event_end = swiftyJsonVar["lautan_berlian"]["result"]["news"]["event_end"].string
                let content = swiftyJsonVar["lautan_berlian"]["result"]["news"]["content"].string
                let thumbnail_url = swiftyJsonVar["lautan_berlian"]["result"]["news"]["thumbnail_url"].string

                self.eventTitle.text = title
                self.eventcontent.text = content
                let addedDate = swiftyJsonVar["lautan_berlian"]["result"]["news"]["date"].string
                
                let convert = addedDate?.toDateString(inputFormat: addedDate!, outputFormat: "dd MMMM")!
                self.eventDate.text = convert
                
                self.eventImg.af_setImage(withURL: URL(string: (thumbnail_url)!)!)
            }
        }

        
    }

    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
