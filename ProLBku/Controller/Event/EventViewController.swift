//
//  EventViewController.swift
//  ProLBku
//
//  Created by mac on 05/07/18.
//  Copyright © 2018 irwan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class EventViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var News = [[String:AnyObject]]()
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            self.tableView.dataSource = self
            self.tableView.delegate = self
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        getNews()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        // Do any additional setup after loading the view.
    }

    func getNews() {
        
        
        let headers: HTTPHeaders = [
            //            "Authorization": "Bearer \(value!)",
            "Accept": "application/json"
        ]
        
        let url = "\(Endpoints.BASE)\(Endpoints.EVENT)"
        
        Alamofire.request(url).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                
                if let resData = swiftyJsonVar["lautan_berlian"]["result"]["events"].arrayObject {
                    self.News = resData as! [[String:AnyObject]]
                }
                if self.News.count > 0 {
                    self.tableView.reloadData()
                }
            }
        }
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.News.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cells", for: indexPath as IndexPath) as! ProductTableViewCell
        var dict = News[indexPath.row]
        let addedDate = dict["date"] as? String
        let convert = addedDate?.toDateString(inputFormat: addedDate!, outputFormat: "dd MMMM")!
        cell.dateEvent.text = convert
        
        cell.titleEvent.text = dict["title"] as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var selectedItem = indexPath
        var dict = News[selectedItem.row]
        let selectID = dict["ID"] as? Int
        print("You selected cell #\(selectID!)!")
        let signUp = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailViewController") as! EventDetailViewController
        signUp.IDEVENT = selectID
        self.present(signUp, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
