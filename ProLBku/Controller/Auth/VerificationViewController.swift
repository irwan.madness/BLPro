//
//  VerificationViewController.swift
//  ProLBku
//
//  Created by mac on 27/06/18.
//  Copyright © 2018 irwan. All rights reserved.
//

import UIKit

class VerificationViewController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var kodeFerifikasi: TLFloatLabelTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func getforgotpassword() {
        let emailaddress = UserDefaults.standard.string(forKey: "EMAIL")
        UserEngine.emailVerifikasi("irwan.madness@gmail.com", code: self.kodeFerifikasi.text!, completionHandler: { (result, error) in
            DispatchQueue.main.async(execute: { () -> Void in
                
                
                
                
                if error == nil{
                    
                    if let base = result as? BaseResponse{
                        print(base.lautan_berlian!)
//                        expired_code
                        
                        print(base.lautan_berlian!)
                        if base.lautan_berlian! == "expired_code" {
                            let alertController = UIAlertController(title: base.lautan_berlian!, message:
                                base.message!, preferredStyle: UIAlertControllerStyle.alert)
                            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default,handler: nil))

                            self.present(alertController, animated: true, completion: nil)
                        } else if base.lautan_berlian! == "expired_code" {
                            let alertController = UIAlertController(title: base.lautan_berlian!, message:
                                base.message!, preferredStyle: UIAlertControllerStyle.alert)
                            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default,handler: nil))
                            
                            self.present(alertController, animated: true, completion: nil)
                        } else {
                            let signUp = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                            self.present(signUp, animated: true, completion: nil)
                        }
                    }
                    
                }
                
                
            })
        })
    }

    @IBAction func verifyEmail(_ sender: Any) {
        getforgotpassword()
    }
    
    @objc func keyboardWillAppear(_ notification: Foundation.Notification) {
        var info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            //            self.bottomConstraint.constant = keyboardFrame.size.height + 20
            let contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardFrame.size.height), 0.0);
            self.scrollView.contentInset = contentInsets
        })
    }
    
    @objc func keyboardWillHide() {
        let contentInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
        self.scrollView.contentInset = contentInsets
    }
}
