//
//  SignUp1ViewController.swift
//  ProLBku
//
//  Created by mac on 12/05/18.
//  Copyright © 2018 irwan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SignUp1ViewController: UIViewController, SSRadioButtonControllerDelegate {

    @IBOutlet weak var lblCabang: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var namadepan: TLFloatLabelTextField!
    @IBOutlet weak var namabelakang: TLFloatLabelTextField!
    @IBOutlet weak var emailTxt: TLFloatLabelTextField!
    @IBOutlet weak var male: UIButton!
    @IBOutlet weak var female: UIButton!
    var rButton : String!
    var radioButtonController: SSRadioButtonsController?
    let regularFont = UIFont.systemFont(ofSize: 16)
    let boldFont = UIFont.boldSystemFont(ofSize: 16)
    var myArray = [String]()
    var id_cabang = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        toggleradioBtn()
        getRadioButton()
        getAllCabang()
        hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
<<<<<<< HEAD
// get all cabang
=======

>>>>>>> master
    func getAllCabang() {
     
        Alamofire.request("https://apidev.lautanberlian.co.id/wp-json/lbcrm/v1/area/cabangs").responseJSON { (response) -> Void in
            if let data = response.data {
                guard let json = try? JSON(data: data) else { return }
<<<<<<< HEAD
                let cabang = json["lautan_berlian"]["result"]["cabang"].arrayValue
                print("banner===",cabang)
                for i in 0..<cabang.count {
                    print("banner[i]==",cabang[i]["name"].string)
                    let allcabang = cabang[i]["name"].string
                    self.myArray.append(allcabang!)
                    let cabangID = cabang[i]["ID"].int
=======
                let banner = json["lautan_berlian"]["result"]["cabang"].arrayValue
                print("banner===",banner)
                for i in 0..<banner.count {
                    print("banner[i]==",banner[i]["name"].string)
                    let imageBanner = banner[i]["name"].string
                    self.myArray.append(imageBanner!)
                    let cabangID = banner[i]["ID"].int
>>>>>>> master
                    print("cabangID==", cabangID!)
                    let myString = String(describing: cabangID)
                    self.id_cabang.append(myString)
                }
                
            }
        }
       
        
    }
    
    func getRadioButton() {
        radioButtonController = SSRadioButtonsController(buttons: male, female)
        radioButtonController!.delegate = self
        radioButtonController!.shouldLetDeSelect = true
        
    }
    
    func toggleradioBtn() {
        if rButton == "Laki-laki" {
            male.isSelected = true
        } else if rButton == "Perempuan" {
            female.isSelected = true
        }
     
    }
    
    func didSelectButton(selectedButton: UIButton?) {
        if male.isSelected{
            self.rButton = "Laki-laki"
        } else if female.isSelected {
            self.rButton = "Perempuan"
        }
    }

    @IBAction func next1(_ sender: Any) {
        not_null()

    }
    
    @IBAction func doneButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    func not_null() {
        if namadepan.text == "" {
            let alertController = UIAlertController(title: " ", message:
                "nama depan tidak boleh kosong", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default,handler: nil))
            self.present(alertController, animated: true, completion: nil)
        } else if namabelakang.text == "" {
            let alertController = UIAlertController(title: " ", message:
                "nama belakang tidak boleh kosong", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default,handler: nil))
            self.present(alertController, animated: true, completion: nil)
        } else if emailTxt.text == "" {
            let alertController = UIAlertController(title: " ", message:
                "Email tidak boleh kosong", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default,handler: nil))
            self.present(alertController, animated: true, completion: nil)
        } else if rButton == nil {
            let alertController = UIAlertController(title: " ", message:
                "Jenis kelamin tidak boleh kosong", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default,handler: nil))
            self.present(alertController, animated: true, completion: nil)
        } else {
            UserDefaults.standard.set(rButton!, forKey: "GENDER")
            UserDefaults.standard.set(namadepan.text, forKey: "NAMADEPAN")
            UserDefaults.standard.set(namabelakang.text, forKey: "NAMABELAKANG")
            UserDefaults.standard.set(emailTxt.text, forKey: "EMAIL")
            
            
            let signUp = self.storyboard?.instantiateViewController(withIdentifier: "SignUp2ViewController") as! SignUp2ViewController
                    self.present(signUp, animated: true, completion: nil)
        }
    }
    
    
    @objc func keyboardWillAppear(_ notification: Foundation.Notification) {
        var info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            //            self.bottomConstraint.constant = keyboardFrame.size.height + 20
            let contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardFrame.size.height), 0.0);
            self.scrollView.contentInset = contentInsets
        })
    }
    
    @objc func keyboardWillHide() {
        let contentInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
        self.scrollView.contentInset = contentInsets
    }
    
    @IBAction func cabangPickerAction(_ sender:UIButton) {
        let blueColor = sender.backgroundColor
        
        let blueAppearance = YBTextPickerAppearanceManager.init(
            pickerTitle         : "Select Fruits",
            titleFont           : boldFont,
            titleTextColor      : .black,
            titleBackground     : .clear,
            searchBarFont       : regularFont,
            searchBarPlaceholder: "Search Fruits",
            closeButtonTitle    : "Cancel",
            closeButtonColor    : .darkGray,
            closeButtonFont     : regularFont,
            doneButtonTitle     : "Done",
            doneButtonColor     : blueColor,
            doneButtonFont      : boldFont,
            checkMarkPosition   : .Right,
            itemCheckedImage    : UIImage(named:"blue_ic_checked"),
            itemUncheckedImage  : UIImage(),
            itemColor           : .black,
            itemFont            : regularFont
        )

        
        
        
        let picker = YBTextPicker.init(with: self.myArray, appearance: blueAppearance,
                                       onCompletion: { (selectedIndexes, selectedValues) in
                                        if let selectedValue = selectedValues.first{
                                            if selectedValue == self.myArray.last!{
                                            }else{
                                                self.lblCabang.text = selectedValue
                                                
                                            }
                                        }else{
                                            
                                        }
                                        
                                       
                                        
        },
                                       onCancel: {
                                        print("Cancelled")
        }
        )
        
      
       
        picker.show(withAnimation: .FromBottom)
        
    }
}
