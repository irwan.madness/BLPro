//
//  CabangViewController.swift
//  ProLBku
//
//  Created by mac on 16/11/18.
//  Copyright © 2018 irwan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireImage
import LCBannerView

class CabangViewController: UIViewController,LCBannerViewDelegate {

    @IBOutlet weak var imageScroller: LCBannerView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var namePC: UILabel!
    @IBOutlet weak var potition: UILabel!
    @IBOutlet weak var imagePC: UIImageView!
    @IBOutlet weak var nameCabang: UILabel!
    @IBOutlet weak var addressCabang: UILabel!
    @IBOutlet weak var phoneCabang: UILabel!
    @IBOutlet weak var faxCabang: UILabel!
    @IBOutlet weak var areaCabang: UILabel!
    @IBOutlet weak var timeOpen: UILabel!
    @IBOutlet weak var operation: UILabel!
    var nomberPhone = [String]()
    var workDay = [String]()
    var imageCabang = [String]()
    var IDCabang: String!
    var iCars : String!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       
        // Do any additional setup after loading the view.
    }
    

    override func viewDidAppear(_ animated: Bool) {
        
        getJaringan()
        
    }
    
    //Return a BannerView
    func AddBannerView(bannerView: UIView, imageUrlArray: [String]) -> LCBannerView{
        let banner = LCBannerView.init(frame: CGRect(x: 0, y: 0, width: bannerView.frame.size.width, height: bannerView.frame.size.height), delegate: self, imageURLs: imageUrlArray, placeholderImageName: "Concert", timeInterval: 3, currentPageIndicatorTintColor: ColorConstant.blueColor, pageIndicatorTintColor: ColorConstant.cyanColor)
        
        banner?.clipsToBounds = true
        return banner!
    }
    
    func getJaringan() {
        
        
        let headers: HTTPHeaders = [
            //            "Authorization": "Bearer \(value!)",
            "Accept": "application/json"
        ]
        let url = "\(Endpoints.BASE)\(Endpoints.DETAIL_CABANG)\(IDCabang!)"
        print(url)
        Alamofire.request(url).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                
                self.addressCabang.text = swiftyJsonVar["lautan_berlian"]["result"]["cabang"]["location"].string
                self.nameCabang.text = swiftyJsonVar["lautan_berlian"]["result"]["cabang"]["name"].string
                let open = swiftyJsonVar["lautan_berlian"]["result"]["cabang"]["open_time"].string
                let close = swiftyJsonVar["lautan_berlian"]["result"]["cabang"]["closed_time"].string
                
                self.timeOpen.text = "\(open ?? "") - \(close ?? "")"
                self.namePC.text = swiftyJsonVar["lautan_berlian"]["result"]["cabang"]["cabang_management"][0]["name"].string
                self.potition.text = swiftyJsonVar["lautan_berlian"]["result"]["cabang"]["cabang_management"][0]["position"].string
                
                self.imagePC.layer.borderWidth = 1.0
                self.imagePC.layer.masksToBounds = false
                self.imagePC.layer.borderColor = UIColor.white.cgColor
                self.imagePC.layer.cornerRadius = self.imagePC.frame.size.width / 2
                self.imagePC.clipsToBounds = true
                let userImage = swiftyJsonVar["lautan_berlian"]["result"]["cabang"]["cabang_management"][0]["profile"].string
                if userImage != nil {
                    self.imagePC.af_setImage(withURL: URL(string: (userImage)!)!)
                }
                
                let cabangPhone = swiftyJsonVar["lautan_berlian"]["result"]["cabang"]["phone"].arrayValue
                for i in 0..<cabangPhone.count{
                    let phone = cabangPhone[i]["phone_number"].string
                    self.nomberPhone.append(phone!)
                    let string = self.nomberPhone.joined(separator: ", ")
                    self.phoneCabang.text = "Phone : \(string)"
                }
                
                let carImage = swiftyJsonVar["lautan_berlian"]["result"]["cabang"]["images"].arrayValue
                for i in 0..<carImage.count{
                    let cars = carImage[i]["image"].string
                    self.imageCabang.append(cars!)
                    self.imageScroller.addSubview(self.AddBannerView(bannerView: self.imageScroller, imageUrlArray: self.imageCabang))
//                    let string = self.imageCars.joined(separator: ", ")
//                    self.phoneCabang.text = "Phone : \(string)"
                }
                
                let cabangFax = swiftyJsonVar["lautan_berlian"]["result"]["cabang"]["fax"].string
                self.faxCabang.text = "Fax : \(cabangFax ?? "")"
                
                self.areaCabang.text = swiftyJsonVar["lautan_berlian"]["result"]["cabang"]["wilayah"].string
                
                let cabangWork = swiftyJsonVar["lautan_berlian"]["result"]["cabang"]["work_day"].arrayValue
                for i in 0..<cabangWork.count{
                    let Work = cabangWork[i].string
                    self.workDay.append(Work ?? "")
                    let string = self.workDay.joined(separator: ", ")
                    self.operation.text = string
                }
            }
        }
    }

    @IBAction func backbutton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func contactUs(_ sender: Any) {
        let contactView = self.storyboard?.instantiateViewController(withIdentifier: "ContactUSViewController") as! ContactUSViewController
        contactView.idCabang = self.IDCabang
        self.present(contactView, animated: true, completion: nil)
    }
}

extension CabangViewController : ImageScrollerDelegate{
    
    func pageChanged(index: Int) {
        //        self.pageIndicatorLabel.text = String(format: "%d/%d", index+1,self.sampleImages.count)
        //        print(index+1)
//        self.pageControl.currentPage = index
    }
}
