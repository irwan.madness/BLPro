//
//  EventResponse.swift
//  ProLBku
//
//  Created by mac on 01/07/18.
//  Copyright © 2018 irwan. All rights reserved.
//

import Foundation
import ObjectMapper

class EventResponse: Mappable {
    
    var code : String?
    var message: String?
    var lautan_berlian: String?
    var name: String?
    var events: [Event]?
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        lautan_berlian <- map["lautan_berlian.code"]
        code <- map["code"]
        message <- map["lautan_berlian.message"]
        events <- map["lautan_berlian.result.events"]
        name <- map[""]
    }
    
}
