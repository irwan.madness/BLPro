//
//  SimKredViewController.swift
//  ProLBku
//
//  Created by reinnatan on 21/06/18.
//  Copyright © 2018 irwan. All rights reserved.
//

import UIKit
import UIDropDown

class SimKredViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var modelCar: UIDropDown!
    @IBOutlet weak var typeCar: UIDropDown!
    @IBOutlet weak var location: UIDropDown!
    
    @IBOutlet weak var listSimulation: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.listSimulation.delegate = self
        self.listSimulation.dataSource = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    // Set the spacing between sections
    //override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
    //    return 10
    //}
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellsim", for: indexPath)
        //cell.textLabel?.text = "Section \(indexPath.section) Row \(indexPath.row)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
   

}

