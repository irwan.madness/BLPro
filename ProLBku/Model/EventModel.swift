//
//  EventModel.swift
//  ProLBku
//
//  Created by mac on 05/07/18.
//  Copyright © 2018 irwan. All rights reserved.
//

import Foundation
import ObjectMapper

class Event: Mappable {
    var ID: Int?
    var title: String?
    var content: String?
    var thumbnail_url: String?
    var event_location_address: String?
    var date: Date?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        ID <- map["ID"]
        title <- map["title"]
        content <- map["content"]
        thumbnail_url <- map["thumbnail_url"]
        event_location_address <- map["event_location_address"]
        date <- map["date"]
    }
}
