//
//  CabangModel.swift
//  ProLBku
//
//  Created by mac on 10/11/18.
//  Copyright © 2018 irwan. All rights reserved.
//

//region model

struct RegionList {
    var ID:String
    var wilayah_id:String
    var name:String
    var region_name:String
    
    
    static var dataSource:[RegionList] {
        
        
        return [
            RegionList.init(ID: "1", wilayah_id: "1", name: "3", region_name: "R1"),
            RegionList.init(ID: "1", wilayah_id: "1", name: "3", region_name: "R2"),
            RegionList.init(ID: "1", wilayah_id: "1", name: "3", region_name: "R1"),
            RegionList.init(ID: "1", wilayah_id: "1", name: "3", region_name: "R2"),
            RegionList.init(ID: "1", wilayah_id: "1", name: "3", region_name: "R3")
            
        ]
    }
}
