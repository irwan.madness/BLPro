//
//  RoundedView.swift
//  ProLBku
//
//  Created by reinnatan on 29/06/18.
//  Copyright © 2018 irwan. All rights reserved.
//

import UIKit

class RoundedView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
     */
    override func draw(_ rect: CGRect) {
        self.layer.cornerRadius = 30
        self.clipsToBounds = true
    }
}
