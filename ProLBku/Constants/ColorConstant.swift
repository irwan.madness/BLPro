//
//  ColorConstant.swift
//  adorableprojects
//
//  Created by mac on 14/05/18.
//  Copyright © 2018 irwan. All rights reserved.
//

import UIKit

struct ColorConstant{
    
    static let blueColor = UIColor(red: 45.0/255.0, green: 72.0/255.0, blue: 158.0/255.0, alpha: 1.0)
    static let cyanColor = UIColor(red: 161.0/255.0, green: 172.0/255.0, blue: 205.0/255.0, alpha: 1.0)

}
