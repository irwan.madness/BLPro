//
//  EventEngine.swift
//  ProLBku
//
//  Created by mac on 05/07/18.
//  Copyright © 2018 irwan. All rights reserved.
//

import UIKit
import ObjectMapper


class EventEngine: Engine {
    class func getAllEnent(_ completionHandler:RequestCompletionHandler?){
        let url = "\(Endpoints.BASE)\(Endpoints.EVENT)"
//        let params = ["email": email,
//                      "code": code]
        self.getRequest(url: url, parameters: nil) { (result, error) in
            if error != nil{
                completionHandler!(result, error)
                return
            }
            let baseResponse = Mapper<BaseResponse>().map(JSONObject: result)
            
            if baseResponse!.lautan_berlian != nil {
                print("CEKBASE== \(baseResponse!.lautan_berlian!)")
                completionHandler!(baseResponse, error)
                return
            }
            let response = Mapper<EventResponse>().map(JSONObject: result)
            if response?.lautan_berlian != nil{
                completionHandler!(response, error)
                print("CEKEVENT== \(response!.lautan_berlian!)")
            }else{
                completionHandler!(result, error)
            }
        }
    }
}
