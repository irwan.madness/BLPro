//
//  ProfileEngine.swift
//  ProLBku
//
//  Created by mac on 30/06/18.
//  Copyright © 2018 irwan. All rights reserved.
//

import UIKit
import ObjectMapper


class ProfileEngine: Engine {

    class func getUser(_ completionHandler:RequestCompletionHandler?){
        let url = "\(Endpoints.BASE)\(Endpoints.USER_PROFILE)"
//        let params = ["": ""]
        self.postRequest(url: url, parameters: nil ,isTokenFromLogin: true) { (result, error) in
            if error != nil{
                completionHandler!(result, error)
                return
            }
            let baseResponse = Mapper<BaseResponse>().map(JSONObject: result)
            completionHandler!(baseResponse, error)
            completionHandler!(result, error)
        }
    }
}
