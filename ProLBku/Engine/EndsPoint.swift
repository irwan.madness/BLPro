//
//  EndsPoint.swift
//  ProLBku
//
//  Created by mac on 09/05/18.
//  Copyright © 2018 irwan. All rights reserved.
//

struct Endpoints{
    
    // dev
    static let BASE = "https://apidev.lautanberlian.co.id/"
    static let LOGIN = "wp-json/lbcrm/v1/login"
    static let FORGOT_PASSWORD = "wp-json/lbcrm/v1/forgot"
    static let SIGNUP_SUBMIT = "wp-json/lbcrm/v1/register/submit"
    static let VERIFY_EMAIL = "wp-json/lbcrm/v1/register/verification"
    static let USER_PROFILE = "wp-json/lbcrm/v1/profile"
    static let EVENT = "wp-json/lbcrm/v1/events"
    static let NEWS = "wp-json/lbcrm/v1/posts/?s=waw"
    static let EVENT_DETAIL = "wp-json/lbcrm/v1/event"
    static let News_DETAIL = "wp-json/lbcrm/v1/post"
    static let KENDARAAN = "wp-json/lbcrm/v1/cars/category/"
    static let MODEL_KENDARAAN = "wp-json/lbcrm/v1/cars/model/"
    static let KENDARAAN_DETAIL = "wp-json/lbcrm/v1/car/"
    static let JARINGAN_CABANG = "wp-json/lbcrm/v1/area"
    static let JARINGAN_WILAYAH = "wp-json/lbcrm/v1/area/wilayahs"
    static let DETAIL_CABANG = "wp-json/lbcrm/v1/cabang/"
    static let CONTACT_CABANG = "/contact"
}
