//
//  ContactUSViewController.swift
//  ProLBku
//
//  Created by reinnatan on 20/06/18.
//  Copyright © 2018 irwan. All rights reserved.
//

import UIKit
import UIDropDown
import Alamofire

class ContactUSViewController: UIViewController {

    @IBOutlet weak var textOrder: TLFloatLabelTextField!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var cabang: RoundUIView!
    @IBOutlet weak var phoneUser: TLFloatLabelTextField!
    @IBOutlet weak var emailUser: TLFloatLabelTextField!
    @IBOutlet weak var nameUser: TLFloatLabelTextField!
    @IBOutlet weak var messageTxt: UITextView!
    @IBOutlet weak var dropDownCabang: UIDropDown!
    var idCabang: String!
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        self.messageTxt.layer.borderWidth = 1.0
        self.messageTxt.layer.borderColor = UIColor.black.cgColor
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    
    func postMessage() {
        
        
        let headers: HTTPHeaders = [
            //            "Authorization": "Bearer \(value!)",
            "Accept": "application/json"
        ]
        let url = "\(Endpoints.BASE)\(Endpoints.DETAIL_CABANG)\(idCabang!)\(Endpoints.CONTACT_CABANG)"
        print(url)
        let parameters: Parameters = ["name": nameUser.text, "email": emailUser.text, "phone": phoneUser.text, "message": messageTxt.text]
        
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON { response in
            print(response)
        }
    }

    @objc func keyboardWillAppear(_ notification: Foundation.Notification) {
        var info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            //            self.bottomConstraint.constant = keyboardFrame.size.height + 20
            let contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardFrame.size.height), 0.0);
            self.scrollView.contentInset = contentInsets
        })
    }
    
    @objc func keyboardWillHide() {
        let contentInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
        self.scrollView.contentInset = contentInsets
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func backbutton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func sendmessage(_ sender: Any) {
        postMessage()
    }
}
